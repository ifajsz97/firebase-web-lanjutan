const functions = require('firebase-functions')
const admin = require('firebase-admin')
const express = require('express')
const cons = require('consolidate')
const hbs = require('express-handlebars')
const app = express()

// Inisialisasi Auntentikasi Admin Firebase ke NodeJS
var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://forumweb-mwa.firebaseio.com"
});

const db = admin.firestore()

app.engine('hbs', hbs({
	extname: 'hbs',
	partialsDir: __dirname + '/views/partials'
}))
app.set('view engine', 'hbs')
app.set('views', './views')

app.get('/forum', function (req, res) {
	var forums = []

	db.collection('forums').orderBy('updated_at', 'desc').limit(5).get()
		.then(snapshot => {
			snapshot.forEach(doc => {
				forums.push(doc.data())
			})

			// untuk pagination
			var lastItem = forums[forums.length - 1]

			res.render('forum', {
				forums: forums,
				lastBlogTime: Date.parse(lastItem.updated_at)
			})

		}).catch(err => {
			console.log('Gagal load daftar forum')
			console.log(err)
		})
})

app.get('/forum/:slug', function (req, res) {
	var forum = null

	db.collection('forums').where('slug', '==', req.params.slug).get()
		.then(snapshot => {
			snapshot.forEach(doc => {
				forum = doc.data()
				forum.id = doc.id
			})

			// Load Reply Forum
			var replies = []
			db.collection('forums').doc(forum.id).collection('replies')
				.orderBy('created_at', 'desc').get()
				.then(snapshot => {
					snapshot.forEach(doc => {
						replies.push({
							data: doc.data(),
							id: doc.id
						})
					})

					res.render('forum-single', {
						forum: forum,
						replies: replies
					})

				}).catch(err => {
					console.log('Gagal Load Reply')
					console.log(err)
				})
		}).catch(err => {
			console.log('Gagal load daftar forum')
			console.log(err)
		})
})

app.get('/forum/older/:lastTime', function (req, res) {
	var forums = []
	var lastTime = new Date(parseInt(req.params.lastTime))

	db.collection('forums').where('updated_at', '<', lastTime).orderBy('updated_at', 'desc').limit(5).get()
		.then(snapshot => {
			snapshot.forEach(doc => {
				forums.push(doc.data())
			})

			// untuk pagination
			var lastItem = forums[forums.length - 1]
			res.render('forum', {
				forums: forums,
				lastBlogTime: Date.parse(lastItem.updated_at)
			})

		}).catch(err => {
			console.log('Gagal load daftar forum')
			console.log(err)
			res.render('forum-empty')
		})
})

exports.app = functions.https.onRequest(app)
