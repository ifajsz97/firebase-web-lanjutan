## Anggota Kelompok

1. Ifan Jaya S. Zalukhu **151111216**
2. Howard  **151111054**
3. Ian Steven Ibrahim **151110709**

---

## Link Online

https://forumweb-mwa.firebaseapp.com/forum

---

## Cara Clone Project

Pastikan anda sudah mengclone projectnya ke komputer anda, setelah itu:

1. Buka command line anda, dan pastikan anda lagi berada di folder project **firebase-web-lanjutan**.
2. Setelah itu, masuk ke folder **functions** bisa dengan perintah **cd functions** di command line anda.
3. Setelah masuk ke folder **function** install package dengan cara **npm install** melalui command line.
4. Lalu (masih di folder **functions**) ketik di command line anda **firebase use forumweb-mwa**.
5. Lalu (masih di folder **functions**) ketik di command line anda **firebase serve --only functions,hosting**.
6. Baru akses servernya di *Localhost:3000/forum*.

---