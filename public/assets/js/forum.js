var auth = firebase.auth()
var db = firebase.firestore()

// ====================
// ===== AUTHENTICATION
// ====================


function login(provider) {
	switch (provider) {
	case 'gmail':
		var provider = new firebase.auth.GoogleAuthProvider();
		break;
	case 'fb':
		var provider = new firebase.auth.FacebookAuthProvider();
		break;
	case 'tw':
		var provider = new firebase.auth.TwitterAuthProvider();
		break;
	default:
		break;
	}

	auth.signInWithPopup(provider).then(function (result) {
		// This gives you a Google Access Token. You can use it to access the Google API.
		var token = result.credential.accessToken;
		// The signed-in user info.
		var user = result.user;

		swal("Berhasil", "Login Berhasil", "success");

		// Cek ID user yg sedang login, sudah disimpan di colletion 'Users' firestore?
		db.collection('users').doc(user.uid).get()
			.then((doc) => {
				// Jika sudah terdaftar
				if (doc.exists) {
					console.log('sudah terdaftar di firestore')
				} else {
					// masukkan UID nya di colletion 'Users' firestore
					db.collection('users').doc(user.uid).set({
						// set displayName ke kolom name
						name: user.displayName
					}).then(function (docRef) {
						console.log('berhasil di daftarkan di firestore')
					}).catch(function (error) {
						console.log('gagal di daftarkan di firestore')
					})
				}
			});
	}).catch(function (error) {
		// Handle Errors here.
		var errorCode = error.code;
		var errorMessage = error.message;
		// The email of the user's account used.
		var email = error.email;
		// The firebase.auth.AuthCredential type that was used.
		var credential = error.credential;

		swal("Gagal", "Gagal login", "error");
	});
}

function logout() {
	swal({
			title: "Yakin ingin logout?",
			text: "Anda harus login lagi untuk tambah forum dan komentar setelah logout",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				auth.signOut()
					.then(function () {
						swal("Berhasil", "Logout Berhasil", "success");
					}).catch(function (err) {
						swal("Gagal", "Gagal logout", "error");
					})
			} else {
				// cancel logout
			}
		});
}

var currentUser = null;
auth.onAuthStateChanged(function (user) {
	if (user) {
		console.log('User sudah login')
		currentUser = user

		// Cek yg sedang login = pembuat_forum
		// jika ya, tampilkan tombol edit di detail forum
		checkForumOwner(user.uid)

		// Cek yg sedang login = pembuat_komentar
		// jika ya, tampikan tombol edit komentar
		checkRepliesOwner(user.uid)

		// handle display login & logout button
		toggleLogin()

	} else {
		console.log('User sudah logout')
		// handle display login & logout button
		toggleLogout()
	}
})


// ====================
// ======= GENERAL FUNC
// ====================
function toggleLogin() {

	// document.getElementById('logout-wrapper').classList.toggle('is-hidden')
	document.querySelector('#login-wrapper').classList.add("is-hidden")
	document.querySelector('#logout-wrapper').classList.remove("is-hidden")
}

function toggleLogout() {

	// document.getElementById('logout-wrapper').classList.toggle('is-hidden')
	document.querySelector('#login-wrapper').classList.remove("is-hidden")
	document.querySelector('#logout-wrapper').classList.add("is-hidden")
}


function checkForumOwner(forum_owner_id) {
	if (document.getElementById('owner_id').value == forum_owner_id) {
		document.getElementById('edit-btn').classList.remove('is-hidden')
	}
}

function checkRepliesOwner(forum_owner_id) {
	var elements = document.getElementsByClassName('replies-button')

	for (var i = 0; i < elements.length; i++) {
		if (elements[i].getAttribute('reply-owner-id') == forum_owner_id) {
			elements[i].classList.remove('is-hidden')
		}
	}
}


// ====================
// ============== FORUM
// ====================

function addForum() {
	var newTitle = document.getElementById('newTitle').value
	var newDesc = document.getElementById('newDesc').value

	//format tanggal ke local
	var tanggal = new Date
	var options = {
		weekday: 'long',
		year: 'numeric',
		month: 'long',
		day: 'numeric'
	}
	var waktu = tanggal.toLocaleDateString('in-In', options);
	var slug = (newTitle + waktu).toLowerCase().replace(/[^\w ]+/g, '').replace(/ +/g, '-')

	if (newTitle.length == 0 || newDesc.length == 0) {
		swal("Gagal", "Judul dan Isi Forum tidak boleh kosong", "info");
	} else {
		db.collection('forums').add({
			title: newTitle,
			slug: slug,
			desc: newDesc.replace("<br />", "\n"),
			created_at: waktu,
			updated_at: waktu,
			user: {
				user_id: currentUser.uid,
				name: currentUser.displayName
			}

		}).then(function (docRef) {
			swal("Berhasil", "Berhasil tambah forum baru", "success");
			window.location = window.location.href + '/' + slug
		}).catch(function (error) {
			swal("Gagal", "Gagal tambah komentar baru", "error");
		})
	}
}

function showEditForm() {
	document.getElementById('editForm').classList.remove('is-hidden')
}

function updateForum(id) {
	//format tanggal ke local
	var tanggal = new Date
	var options = {
		weekday: 'long',
		year: 'numeric',
		month: 'long',
		day: 'numeric'
	}
	var waktu = tanggal.toLocaleDateString('in-In', options);

	var judulUpdateForum = document.getElementById('updateTitle').value
	var descUpdateForum = document.getElementById('updateDesc').value.replace("<br />", "\n")

	if (judulUpdateForum.length < 1 || descUpdateForum.length < 1) {
		swal("Gagal", "Judul dan Isi Forum tidak boleh kosong", "info");
	} else {
		db.collection('forums').doc(id).set({
				title: judulUpdateForum,
				desc: descUpdateForum,
				updated_at: waktu
			}, {
				merge: true
			})
			.then(function () {
				swal("Berhasil", "Berhasil update forum", "success")
				location.reload()
			}).catch(function (error) {
				swal("Gagal", "Gagal tambah update forum", "error")
			})
	}
}

// ====================
// ============== REPLY
// ====================

function addReply(id) {
	var newReply = document.getElementById('replyBox').value
	var forumsRef = db.collection('forums').doc(id)

	if (newReply.length == 0 || newReply.length == null || newReply.length < 1) {
		swal("Gagal", "Komentar tidak boleh kosong", "info");
	} else {
		forumsRef.collection('replies').add({
			desc: newReply.replace("<br />", "\n"),
			created_at: new Date(),
			updated_at: new Date,

			user: {
				user_id: currentUser.uid,
				name: currentUser.displayName
			}

		}).then(function (docRef) {
			swal("Berhasil", "Berhasil tambah komentar baru", "success");
			location.reload()
		}).catch(function (error) {
			swal("Gagal", "Gagal tambah komentar baru", "error");
		})
	}
}


var activeReplyId = ''

function editReply(replyId, prevDesc) {
	activeReplyId = replyId
	document.getElementById('replyEditBox').value = prevDesc
	document.getElementById('formEditReplay').classList.remove('is-hidden')
}

function updateReply() {
	var forumId = document.getElementById('forum_id').value
	var newUpdateReply = document.getElementById('replyEditBox').value

	var dbRef = db.collection('forums').doc(forumId).collection('replies').doc(activeReplyId)

	if (newUpdateReply.length < 1) {
		swal("Gagal", "Isi komentar tidak boleh kosong", "info");
	} else {
		dbRef.set({
				desc: newUpdateReply.replace("<br />", "\n"),
				updated_at: new Date()
			}, {
				merge: true
			})
			.then(function () {
				swal("Berhasil", "Berhasil update Komentar", "success");
				location.reload()
			})
			.catch(function (error) {
				swal("Gagal", "Gagal update komentar", "error");
				console.log("Reply gagal diupdate " + error)
			})
	}

}
